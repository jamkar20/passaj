import React, {Component} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {Input,Card,Icon} from 'react-native-elements'
import {Button,Content,Container} from 'native-base'

export default class Stores extends Component {
    render() {
        return (
            <Container >
                <Content>
                    <Card
                        title='شتابدهنده نوآوری پیشگامان'
                        image={{
                            uri:'http://yazd.isna.ir//Files/News/Image/139611/68653.jpg'
                        }}>
                        <Text style={{marginBottom: 10}}>
                            The idea with React Native Elements is more about component structure than actual design.
                        </Text>
                        <Button
                            icon={<Icon name='code' color='#ffffff' />}
                            backgroundColor='#03A9F4'
                            fontFamily='Lato'
                            buttonStyle={{borderRadius: 0, marginLeft: 0, marginRight: 0, marginBottom: 0}}
                            title='VIEW NOW' />
                    </Card>
                    <Card
                        title='شتابدهنده نوآوری پیشگامان'
                        image={{
                            uri:'http://yazd.isna.ir//Files/News/Image/139611/68653.jpg'
                        }}>
                        <Text style={{marginBottom: 10}}>
                            The idea with React Native Elements is more about component structure than actual design.
                        </Text>
                        <Button
                            icon={<Icon name='code' color='#ffffff' />}
                            backgroundColor='#03A9F4'
                            fontFamily='Lato'
                            buttonStyle={{borderRadius: 0, marginLeft: 0, marginRight: 0, marginBottom: 0}}
                            title='VIEW NOW' />
                    </Card>
                    <Card
                        title='شتابدهنده نوآوری پیشگامان'
                        image={{
                            uri:'http://yazd.isna.ir//Files/News/Image/139611/68653.jpg'
                        }}>
                        <Text style={{marginBottom: 10}}>
                            The idea with React Native Elements is more about component structure than actual design.
                        </Text>
                        <Button
                            icon={<Icon name='code' color='#ffffff' />}
                            backgroundColor='#03A9F4'
                            fontFamily='Lato'
                            buttonStyle={{borderRadius: 0, marginLeft: 0, marginRight: 0, marginBottom: 0}}
                            title='VIEW NOW' />
                    </Card>

                </Content>

            </Container>
        )
    }

}
Styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    textButton: {
        fontFamily: 'iransans',
        color: 'white',
        fontSize: 18,
        textAlign: 'center'
    }
})
