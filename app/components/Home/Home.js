import React, {Component} from 'react';
import {StyleSheet, Text, View,PermissionsAndroid} from 'react-native';
import {Input, Header, Icon, colors} from 'react-native-elements'
import {Button} from 'native-base'
import Stores from "../Stores/Stores";
import NotificationPopup from 'react-native-push-notification-popup'

var PushNotification = require('react-native-push-notification')
PushNotification.configure({

    // (optional) Called when Token is generated (iOS and Android)
    onRegister: function (token) {
        console.log('TOKEN:', token);
    },

    // (required) Called when a remote or local notification is opened or received
    onNotification: function (notification) {
        console.log('NOTIFICATION:', notification);

        // process the notification

    },

    // ANDROID ONLY: GCM or FCM Sender ID (product_number) (optional - not required for local notifications, but is need to receive remote push notifications)
    senderID: "YOUR GCM (OR FCM) SENDER ID",

    // IOS ONLY (optional): default: all - Permissions to register.
    permissions: {
        alert: true,
        badge: true,
        sound: true
    },

    // Should the initial notification be popped automatically
    // default: true
    popInitialNotification: false,

    /**
     * (optional) default: true
     * - Specified if permissions (ios) and token (android and ios) will requested or not,
     * - if not, you must call PushNotificationsHandler.requestPermissions() later
     */
    requestPermissions: true,
});
async function requestCameraPermission() {
    try {
        const granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
            {
                'title': 'Cool Photo App Camera Permission',
                'message': 'Cool Photo App needs access to your camera ' +
                'so you can take awesome pictures.'
            }
        )
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            console.log("You can use the camera")
        } else {
            console.log("Camera permission denied")
        }
    } catch (err) {
        console.warn(err)
    }
}
export default class Home extends Component {
    constructor(props) {
        super(props)

    }

    state = {
        selectedKey: 0,
        content: <Text>خوش آمدید</Text>,
        latitude:null,
        longitude:  null,
        error: null,

    }
    onPressTab = (num) => {
        this.setState({selectedKey: num})
        this.setState({content: this.selectContent(num)})

    }
    selectContent = (num) => {
        switch (num) {
            case 1:
                return (<Stores/>)
            case 2:
                return (<Text>سفارشات</Text>)
            case 3:
                return (<Text>کیف پول</Text>)
        }
    }
    handleCartButton = () => {
        PushNotification.localNotification({
            title: "پاساژ بازار همراه تو",
            message: "اولین پیام پاساژ",
            autoCancel: false,
            bigText: "My big text that will be shown when notification is expanded", // (optional) default: "message" prop
            subText: "This is a subText", // (optional) default: none,
            actions: '["Yes", "No"]',
        })
        this.popup.show({
            onPress: function () {

            },
            appTitle:'پاساژ',
            timeText: 'حالا',
            title: 'پاساژ بازار همراه تو',
            body: 'موقعیت شما : '+this.state.latitude+'--'+this.state.longitude,
            font:'iransans'
        });

    }
    handleSearchButton = () => {
        PushNotification.localNotificationSchedule({
            message: "پیام زمان دار", // (required)
            date: new Date(Date.now() + 1000 * 6) // in 60 secs
        });
    }

    componentDidMount() {
        requestCameraPermission()
        this.watchId = navigator.geolocation.watchPosition(
            (position) => {
                this.setState({
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude,
                    error: null,
                });
            },
            (error) => this.setState({ error: error.message }),
            { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000, distanceFilter: 10 },
        );
    }

    componentWillUnmount() {
        navigator.geolocation.clearWatch(this.watchId);
    }


    render() {
        return (
            <View style={Styles.container}>
                <View style={{alignItems:'center',flexDirection:'column'}}>
                    <NotificationPopup ref={ref => this.popup = ref} />

                </View>
                <Header
                    leftComponent={<View style={{flexDirection: 'row'}}>
                        <Button transparent rounded style={{width: 48, height: 48, justifyContent: 'center'}}
                                onPress={this.handleCartButton}>
                            <Icon name='md-cart'
                                  type='ionicon'
                                  color={'#fff'}
                                  size={32}
                            />
                        </Button>
                        <Button transparent rounded style={{width: 48, height: 48, justifyContent: 'center'}}
                                onPress={this.handleSearchButton}>
                            <Icon name='md-search'
                                  type='ionicon'
                                  color={'#fff'}
                                  size={32}
                            />
                        </Button>
                    </View>}
                    centerComponent={{
                        text: 'پاساژ', style: {
                            fontFamily: 'iransans',
                            color: 'white',
                            fontSize: 18,
                            textAlign: 'center'
                        }
                    }}
                    rightComponent={<Button transparent rounded onPress={() => this.props.openDrawer()}
                                            style={{width: 48, height: 48, justifyContent: 'center'}}>
                        <Icon name='md-menu'
                              type='ionicon'
                              color={'#fff'}
                              size={32}
                        />
                    </Button>}
                    outerContainerStyles={{height: 64}}
                    placement={'center'}
                />
                <View style={{flex: 1, justifyContent: 'center'}}>
                    {this.state.content}
                </View>
                <View style={{
                    flexDirection: 'row',
                    backgroundColor: colors.primary,
                    height: 56,
                    justifyContent: 'space-around'
                }}>
                    <Button transparent rounded style={{width: 48, height: 48, justifyContent: 'center'}}
                            onPress={() => {
                                this.onPressTab(1)
                            }}>
                        <Icon name='store'
                              color={(this.state.selectedKey === 1) ? '#fff' : colors.grey4}
                              size={32}
                        />
                    </Button>
                    <Button transparent rounded style={{width: 48, height: 48, justifyContent: 'center'}}
                            onPress={() => {
                                this.onPressTab(2)
                            }}>
                        <Icon name='assessment'
                              color={(this.state.selectedKey === 2) ? '#fff' : colors.grey4}
                              size={32}
                        />
                    </Button>
                    <Button transparent rounded style={{width: 48, height: 48, justifyContent: 'center'}}
                            onPress={() => {
                                this.onPressTab(3)
                            }}>
                        <Icon name='account-balance-wallet'
                              color={(this.state.selectedKey === 3) ? '#fff' : colors.grey4}
                              size={32}
                        />
                    </Button>
                </View>
            </View>
        )
    }

}
Styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'stretch'

    },
    textButton: {
        fontFamily: 'iransans',
        color: 'white',
        fontSize: 18,
        textAlign: 'center'
    }
})
