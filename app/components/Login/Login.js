import React, {Component} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {Input} from 'react-native-elements'
import {Button} from 'native-base'

export default class Login extends Component {
    render() {
        return (
            <View style={Styles.container}>
                <Input placeholder={'تلفن همراه'} fontFamily={'iransans'} inputStyle={{height: 48,textAlign:'center'}}
                       textcontenttype={'telephoneNumber'} keyboardType={'phone-pad'}/>
                <View style={{paddingTop: 15}}>
                    <Button success style={{width: 120, justifyContent: 'center'}}>
                        <Text style={Styles.textButton}>
                            ورود
                        </Text>
                    </Button>
                </View>

            </View>
        )
    }

}
Styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    textButton: {
        fontFamily: 'iransans',
        color: 'white',
        fontSize: 18,
        textAlign: 'center'
    }
})
