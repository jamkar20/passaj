/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,Dimensions,TouchableOpacity} from 'react-native';
import {Avatar,Icon} from 'react-native-elements'
import Login from "./app/components/Login/Login";
import Home from "./app/components/Home/Home";
import Drawer from 'react-native-drawer'
const Width_Window= Dimensions.get('window').width

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

type Props = {};
export default class App extends Component<Props> {
      closeControlPanel = () => {
          this._drawer.close()
      };
      openControlPanel = () => {
          this._drawer.open()
      };
    menu = (
        <View style={{flex: 1, backgroundColor: '#fff',}}>
            <View style={{
                height: 128,
                alignItems: 'center',
                justifyContent: 'space-around',
                backgroundColor: '#ffcc00'
            }}>
                <Avatar size="large"
                        rounded
                        title={"JK"}
                        onPress={() => console.log("Works!")}
                        activeOpacity={0.7}/>
                <Text style={{fontSize: 20, fontFamily: "iransans",}}>جمال کارگر شورکی</Text>


            </View>
            <TouchableOpacity onPress={() => {
                this.closeControlPanel()
            }} style={styles.menuButton}>
                <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end'}}>
                    <Text style={styles.menuText} >تنظیمات</Text>
                    <Icon style={styles.menuIcon}  name={"ios-settings-outline"} type={'ionicon'} size={32}/>
                </View>

            </TouchableOpacity>
            <TouchableOpacity onPress={() => {
                this.closeControlPanel()
            }} style={styles.menuButton}>
                <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end'}}>
                    <Text style={styles.menuText}>راهنما</Text>
                    <Icon style={styles.menuIcon} name={"ios-help-circle-outline"} type={'ionicon'} size={32}/>
                </View>

            </TouchableOpacity>
            <TouchableOpacity onPress={() => {
                this.closeControlPanel()
            }} style={styles.menuButton}>
                <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end'}}>
                    <Text style={styles.menuText}>درباره ما</Text>
                    <Icon style={styles.menuIcon} name={"ios-information-circle-outline"} type={'ionicon'} size={32}/>
                </View>

            </TouchableOpacity>
            <TouchableOpacity onPress={() => {
                this.closeControlPanel()
            }} style={styles.menuButton}>
                <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end'}}>
                    <Text style={styles.menuText}>تماس با ما</Text>
                    <Icon style={styles.menuIcon} name={"ios-call-outline"} type={'ionicon'} size={32}/>
                </View>

            </TouchableOpacity>
        </View>
    )
      render () {
          return (
              <Drawer
                  ref={(ref) => this._drawer = ref}
                  content={this.menu}
                  openDrawerOffset={(viewport) => viewport.width - Width_Window*0.66}
                  type={'overlay'}
                  side={'right'}
                  tapToClose={true}
                  elevation={10}
                  negotiatePan={true}
                  captureGestures={true}
              >
                  <Home openDrawer={this.openControlPanel} />
              </Drawer>
          )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
    menuText: {
        paddingRight: 32,
        paddingTop: 16,
        fontSize: 18,
        fontFamily: "iransans",
    },
    menuIcon: {
        paddingRight: 16,
        paddingTop: 20,

    },
    menuButton:{
      paddingRight:10
    }
});
